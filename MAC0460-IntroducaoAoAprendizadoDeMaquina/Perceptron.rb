# this is a toy code

class Perceptron
  def initialize(data,w)
    @data,@w = data,w
    run
  end

  private

    def sign(x)
      return 1 if x >= 0
      -1
    end
    
    def dot(u,v)
      r = 0
      u.size.times do |i|
        r += (u[i] * v[i])
      end
      r
    end
    
    def arraySum(u,v)
      return nil if u.size != v.size
      r = []
      u.size.times do |i|
        r[i] = u[i] + v[i]
      end

      r
    end

    def adjustWeights(xt)
      y = xt[-1]
      aux = xt[0..-2].map{ |xti| y * xti }
      @w = arraySum @w, aux
    end

    def run
      changed,j = false, 0
      loop do
        changed = false
        @data.size.times do |i|
          wi = dot @w, @data[i][0..-2]
          changed = true and adjustWeights(@data[i]) unless
              sign(wi).eql? sign(@data[i][-1])
        end
        break if (!changed or j > 30)
      end

      puts "Final w = #{@w}"
    end
end

data = []
data[0] = [1, 0, 0, -1]
data[1] = [1, 0, 1,  1]
data[2] = [1, 1, 0,  1]
data[3] = [1, 1, 1,  1]

w = [0.5, -1, 1]

p = Perceptron.new data, w
